/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package loader;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;
/**
 *
 * @author nerd
 */
public class Serial {
    private byte buffer[];
    private int head;
    private int tail;
    private final int max_buffer = 1024;
    
    
    private int timeout = 5000;
    
    private int baudrate;
    private int databits;
    private int stopbit;
    private int parity;
    private String portname;
    
    
    private SerialPort serial;
    private PrintStream printer;
    
    public static final char PARITY_NONE = 'N';
    public static final char PARITY_ODD = 'O';
    public static final char PARITY_EVEN = 'E';
    public static final char PARITY_MARK = 'M';
    public static final char PARITY_SPACE = 'S';
    
    public Serial(String port){
        init(port,9600,8,1,'N');
    }
    
    public Serial(String port, int baudrate){
        init(port, baudrate, 8, 1, 'N');
    }
        
    public Serial(String port, int baudrate, int databit, String parity, int stopbit) {
        parity = parity.toUpperCase();
        char parseParity = parity.charAt(0);
        init(port, baudrate, databit, stopbit, parseParity);
    }
    
    public Serial(String port, int baudrate, int databit, int stopbit, char parity){
        init(port, baudrate, databit, stopbit, parity);
    }
    
    private void init(String port, int baud, int databits, int stop, char parity){
        this.portname = port;
        this.baudrate = baud;
        this.databits = databits;
        this.stopbit = stop;
        String parityCode = "NOEMS";
        int _parity = parityCode.indexOf(parity);
        this.parity = _parity;
        buffer = new byte[this.max_buffer];
        this.head = 0;
        this.tail = 0;
        this.printer = System.err;
        serial = new SerialPort(port);
        try {
            serial.openPort();
            if(serial.isOpened()){
                serial.setParams(baud, databits, stop, _parity);
                serial.setDTR(false);
                serial.setRTS(false);
                serial.setEventsMask(SerialPort.MASK_RXCHAR);
                serial.addEventListener(new SerialPortEventListener() {
                    @Override
                    public void serialEvent(SerialPortEvent spe) {
                        serialEventRoutine(spe);
                    }
                },SerialPort.MASK_RXCHAR);
            }
        } catch (SerialPortException ex) {
            this.printer.println("fail to open port");
        }
    }
    
    public void setLogger(PrintStream printer){
    
    }
    
    public int available(){
        if(this.head == this.tail)return 0;
        if(this.head>this.tail)return this.head - this.tail;
        else return this.max_buffer - this.tail + this.head;
    }
    
    public boolean isConnected(){
        return this.serial.isOpened();
    }
    
    public static String[] portList(){
        return SerialPortList.getPortNames();
    }
    
    public void setPrinter(PrintStream printer){
        this.printer = printer;
    }
    
    public void setTimeout(int timeout){
        this.timeout = timeout;
    }
    
    public byte[] read(int count){
        return read(count, this.timeout);
    }
    
    public byte[] read(int count, int timeout){
        byte result[] = new byte[count];
        int i = 0;
        long start = System.currentTimeMillis();
        while(i<count){
            if(available()>0){
                result[i] = (byte)read();
                i++;
            }
            if((System.currentTimeMillis() - start) > timeout){
                result = Arrays.copyOfRange(result, 0, i);
                break;
            }
        }
        return result;
    }
    
    public int read(){
        int result = -1;
        synchronized(this.buffer){
            if(this.tail!=this.head){
                result = this.buffer[this.tail];
                result = result & 0xFF;
                this.tail++;
                if(this.tail == this.max_buffer)this.tail = 0;
            }
        }
        return result;
    }
    
    public boolean reOpen(){
        if(serial!=null){
            if(serial.isOpened()){
                try {
                    serial.closePort();
                } catch (SerialPortException ex) {
                }
            }
            try {
                serial = null;
                serial = new SerialPort(this.portname);
                serial.openPort();
                serial.setParams(this.baudrate, this.databits, this.stopbit, this.parity);
                serial.setDTR(false);
                serial.setRTS(false);
                if(serial.isOpened()){
                    return true;
                }
            } catch (SerialPortException ex) {
            } 
//            }
        }else{
//            Control.sleep(10);
            serial = new SerialPort(this.portname);
            try {
                serial.openPort();
                serial.setParams(this.baudrate, this.databits, this.stopbit, this.parity);
                serial.setDTR(false);
                serial.setRTS(false);
                if(serial.isOpened()){
                    return true;
                }
            } catch (SerialPortException ex) {
            }
        }
        return false;
    }
    
    public void close(){
        if(serial!=null)try {
            serial.closePort();
        } catch (SerialPortException ex) {
            this.printer.println("unable to close port");
        }
    }
        
    public void setDTR(boolean state){
        try {
            serial.setDTR(state);
        } catch (SerialPortException ex) {
            this.printer.println("Fail to Set DTR");
        }
    }
    
    public void setRTS(boolean state){
        try {
            serial.setRTS(state);
        } catch (SerialPortException ex) {
            this.printer.println("Fail to Set DTR");
        }
    }
    
    public boolean isCD(){
        boolean result = false;
        try {
            result = serial.isRLSD();
        } catch (SerialPortException ex) {
            this.printer.println("CTS error");
        }
        return result;
    }

    public boolean isCTS(){
        boolean result = false;
        try {
            result = serial.isCTS();
        } catch (SerialPortException ex) {
            this.printer.println("CTS error");
        }
        return result;
    }

    public boolean isDSR(){
        boolean result = false;
        try {
            result = serial.isDSR();
        } catch (SerialPortException ex) {
            this.printer.println("DSR error");
        }
        return result;
    }

    public boolean isRI(){
        boolean result = false;
        try {
            result = serial.isRING();
        } catch (SerialPortException ex) {
            this.printer.println("RI error");
        }
        return result;
    }
    
    public void writeBytes(byte[] data){
        try {
            serial.writeBytes(data);
        } catch (SerialPortException ex) {
            this.printer.println("error writing");
        }
    }
    
    public void write(byte data){
        try {
            serial.writeByte(data);
        } catch (SerialPortException ex) {
            this.printer.println("error writing");
        }
    }
    
    public void write(byte[] data){
        try {
            serial.writeBytes(data);
        } catch (SerialPortException ex) {
            this.printer.println("error writing");
        }
    }
    
    public void write(String data){
        try {
            serial.writeString(data);
        } catch (SerialPortException ex) {
            this.printer.println("error writing");
        }
    }
    
    private void serialEventRoutine(SerialPortEvent event){
        if (event.getEventType() == SerialPortEvent.RXCHAR) {
            int toRead;
            try {
                while (0 < (toRead = serial.getInputBufferBytesCount())) {
                    synchronized(this.buffer){
                        byte dataEvent[] = serial.readBytes(toRead);
                        for(byte b:dataEvent){
                            if(this.head+1 == this.tail)this.tail++;
                            if(this.head+1 == max_buffer && this.tail ==0)this.tail++;
                            this.buffer[this.head] = b;
                            this.head++;
                            if(this.head == this.max_buffer)this.head = 0;
                        }
                    }
                    
                }
            } catch (SerialPortException ex) {
                Logger.getLogger(Serial.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
   
}
